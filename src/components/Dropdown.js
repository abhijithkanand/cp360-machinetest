import * as React from 'react';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import Switch from '@mui/material/Switch';
import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';

import Multiselect from 'multiselect-react-dropdown';

export default function Dropdown(props) {
    const [selectedValue, setSelectedValue] = React.useState(null)
    const [selectedList, setSelectedList] = React.useState(null)
    const [multiSelect, setMultiSelect] = React.useState(true)
    const [searchable, setSearchable] = React.useState(true)
    const [options, setOptions] = React.useState([])
    const [key, setKey] = React.useState(1)


    const onSelect = (selectedList, selectedItem) => {
        //function for handle multiple selection
        setSelectedValue(selectedItem)
        setSelectedList(selectedList)
    }

    const onRemove = (selectedList, removedItem) => { setSelectedList(selectedList) }

    const handleMultiSelect = () => {
        //function for enabling/ disabling the multi select
        setMultiSelect(!multiSelect)
        setSelectedValue(null)
        setSelectedList(null)
    }

    const handleOptions = async (data) => {
        //function for set the options according to type
        if (data[0].title) setOptions(data)
        else {
            let temOptions = []
            await Promise.all(
                data.map((option) => {
                    temOptions.push({ title: option, id: option })
                })
            )
            setOptions(temOptions)
        }
        setSelectedValue(null)
        setSelectedList(null)
        setKey(key++)
    }

    const handleSearchable = (currentValue) => {
        //function for enabling/ disabling the search
        if (currentValue) setMultiSelect(false)
        setSearchable(!currentValue)
    }



    const submit = () => {
        //function fot submitting data
        alert("Data submitted.")
    }
    const clear = () => {
        //function for clearing data
        setSelectedValue(null)
        setSelectedList(null)
    }
    React.useEffect(() => { if (props.options) handleOptions(props.options) }, [props.options])
    React.useEffect(() => { clear() }, [])
    React.useEffect(() => { clear() }, [options])

    return (
        <FormGroup>
            <FormControlLabel labelPlacement="start" control={<Switch checked={searchable} onClick={() => handleSearchable(searchable)} />} label="Searchable" />
            <FormControlLabel labelPlacement="start" disabled={!searchable} control={<Switch checked={multiSelect} onClick={handleMultiSelect} />} label="Multi Select" />
            {
                searchable ?
                    <Multiselect
                        key={key}
                        selectedValues={multiSelect ? selectedList : selectedValue}
                        options={options}
                        onSelect={onSelect}
                        onRemove={onRemove}
                        displayValue="title"
                        showCheckbox={multiSelect}
                        singleSelect={!multiSelect}
                        placeholder=""

                    /> :
                    <FormControl fullWidth>
                        <Select
                            style={{ height: "32px" }}
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={selectedValue}
                            label="Age"
                            onChange={(e) => setSelectedValue(e.target.value)}
                        >
                            {options.map((item) => {
                                return (<MenuItem value={item.id}>{item.title}</MenuItem>)
                            })}
                        </Select>
                    </FormControl>
            }
            {
                selectedList || selectedValue ?
                    <Stack
                        sx={{ pt: 4 }}
                        direction="row"
                        spacing={2}
                        justifyContent="center"
                    >
                        <Button onClick={submit} variant="contained">Submit</Button>
                        <Button onClick={clear} variant="outlined">Clear</Button>
                    </Stack>
                    :
                    null
            }


        </FormGroup>
    );
}