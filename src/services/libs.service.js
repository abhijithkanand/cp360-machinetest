const regex = {
    alphanumeric: /^[A-Za-z0-9 _]*[A-Za-z0-9][A-Za-z0-9 _]*$/,
    float: /^\d*\.?\d*$/,
    alphabet: /^[a-zA-Z ]*$/,
    email: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    number: /^[0-9\b]+$/,
    all: /.*/,
}

export default {
    success: (message, data) => {
        //function for retuning success response 
        return ({
            status: true,
            msg: message ? message : "Success",
            data: data
        })
    },
    error: (message) => {
        //function for retuning error response 
        return ({
            status: false,
            msg: message ? message : "Internal Error",
        })
    },
    validate: (data, type, length) => {
        //function to validate given data 
        if (data.length <= length && (data === "" || regex[type].test(data.trim()))) {
            return true
        } else return false
    },
    getUid: async () => {
        //function to generate unique id 
        let head = Date.now().toString(36);
        let tail = Math.random().toString(36).substring(2, 7);
        return head + tail
    },
}