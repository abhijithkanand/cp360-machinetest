import libs from "./libs.service"
import validate from "./validation.service"


export default {
    signIn: async (userData) => {
        //function to sign user 
        try {
            let resp = await validate.signIn(userData)
            if (!resp.status) return libs.error(resp.msg)
            let { password } = userData
            let numberArr = password.split("")
            let sum = await numberArr.reduce(function (total, num) { return parseInt(total) + parseInt(num) })
            if (sum !== 10) return libs.error("Invalid credentials.")
            let token = await libs.getUid()
            sessionStorage.setItem("cp_key", token)
            return libs.success("User validated.")
        } catch (error) {
            console.log("User sign in service broken :", error)
            return libs.error()
        }
    },
    validateUser: async () => {
        try {
            if (sessionStorage.getItem('cp_key')) return libs.success()
            return libs.error("Unable to verify user.")
        } catch (error) {
            return libs.error("Unable to verify user.")
        }
    }

}