import libs from "./libs.service"

export default {
    signIn: async (userData) => {
        //function to validate user data 
        try {
            let { email, password } = userData
            if (!libs.validate(email, "email", email.length)) return libs.error("Please enter valid email address.")
            if (!libs.validate(password, "number", password.length)) return libs.error("Please enter password.")
            return libs.success("User data validated.")
        } catch (error) {
            console.log("User sign in validation service broken :", error)
            return libs.error()
        }
    }
}