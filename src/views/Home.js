import * as React from 'react';
import { useNavigate } from "react-router-dom";
import AppBar from '@mui/material/AppBar';
import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';

import Dropdown from '../components/Dropdown'

import user from '../services/user.service';



export default function Home() {
    const navigate = useNavigate();
    const [options, setOptions] = React.useState([])
    let options1 = [
        { "title": "React", "id": "react" },
        { "title": "Angular", "id": "angular" },
        { "title": "Vue", "id": "vue" },
        { "title": "Ember", "id": "ember" }
    ]
    let options2 = ['red', 'yellow', 'green', 'blue']

    const changeOptions = (type) => {
        //function for changing options 
        if (type === 1) setOptions(options1)
        else setOptions(options2)
    }
    React.useEffect(() => { changeOptions(1); verifyUser() }, [])

    const verifyUser = async () => {
        //function to verify user 
        let userStatus = await user.validateUser()
        if (!userStatus.status) navigate("/");
    }

    return (
        <div>
            <AppBar position="relative">
                <Toolbar>
                    <Typography variant="h6" color="inherit" noWrap>Home</Typography>
                </Toolbar>
            </AppBar>
            <main>
                <Box sx={{ pt: 8, pb: 6, }}>
                    <Container maxWidth="sm">
                        <Stack
                            sx={{ pt: 4 }}
                            direction="row"
                            spacing={2}
                            justifyContent="center"
                        >
                            <Button onClick={() => changeOptions(1)} size='small' variant="outlined">Options 1</Button>
                            <Button onClick={() => changeOptions(2)} size='small' variant="outlined">Options 2 </Button>
                        </Stack>
                        <Dropdown options={options} />
                    </Container>
                </Box>
            </main>
        </div>
    );
}