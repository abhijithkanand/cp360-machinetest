import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import Snackbar from '@mui/material/Snackbar';
import { useNavigate } from "react-router-dom";



import user from '../services/user.service';


export default function SignIn() {
    const navigate = useNavigate();
    const [open, setOpen] = React.useState(false);
    const [snackMsg, setSnackMsg] = React.useState("");

    const handleClose = () => { setOpen(false); };
    const openSnackBar = (msg) => {
        setSnackMsg(msg)
        setOpen(true);
    };


    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = new FormData(event.currentTarget);
        let resp = await user.signIn({ email: data.get('email'), password: data.get('password') })
        if (!resp.status) openSnackBar(resp.msg)
        else navigate("/home");

    };

    return (
        <div>
            <Container component="main" maxWidth="xs">
                <Box sx={{ marginTop: 8, display: 'flex', flexDirection: 'column', alignItems: 'center', }}>
                    <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }} />
                    <Typography component="h1" variant="h5">Sign in</Typography>
                    <Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
                        <TextField margin="normal" required fullWidth id="email" label="Email Address" name="email" autoComplete="email" autoFocus />
                        <TextField margin="normal" required fullWidth name="password" label="Password" type="password" id="password" autoComplete="current-password" />
                        <Button type="submit" fullWidth variant="contained" sx={{ mt: 3, mb: 2 }}>Sign In</Button>
                    </Box>
                </Box>
            </Container>
            <Snackbar
                open={open}
                anchorOrigin={{
                    vertical: "bottom",
                    horizontal: "right",
                }}
                autoHideDuration={1000}
                severity="error"
                onClose={handleClose}
                message={snackMsg}
            />
        </div>
    );
}